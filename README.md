# Vine collection app - Angular 2

## Description
This repository holds Vine Collection app.
Even though it was long time ago I was using Angular, I have decided to give it a go and use Angular 2 for this project especially because thats what you are using and it will not make a lot of sense to do it in Vue.js.

It means also it took some longer time for me to do it and unfortunately I have not managed to do everything as I wanted because of time.
So this is what I have managed to build:
- front page with list of vines
- Vine detail page, which comes if you click "Details"
- "Add new vine" page


I took inspiration from the theme I found on internet - http://luxurywine.themerex.net
I wrote all SCSS and HTML by myself.
All components are placed in dev folder.
All SCSS code are placed in "assets/scss" folder.
Images are in "src/img" and "src/vine-images" folder.

I have used Firebase database to save my data at and I wrote a service which allows our application to
call functions:
- getVines()
- getSingleVine)()
- addNewVine()


On the Add new vine page you have to add image url. I have no more time to add an image upload functionality, so thats why we need to add image url manually.
For test purposes you can use one of the existing image urls:
- ./src/vine-images/carignan.png
- ./src/vine-images/cabernet-sauvignon.png
- ./src/vine-images/cold-wind.png

I wanted also to add these functionalities, but time is out now:
 - to edit vine
 - delete vine
 - add sorting options
 - add notes for each vine
 
Basically application is working properly. One thing you need to remember is to add existing img url when adding new vine.
I did not have time to do mobile styles at all, so that part is missing too.



## Based on Angular 2 Beta Boilerplate by mschwarzmueller
I have used a boilerplate adjusted by another person.
It is derived from the official Angular 2 Documentation which can be found [here](https://angular.io/docs/ts/latest/quickstart.html).
```
git clone https://github.com/mschwarzmueller/angular-2-beta-boilerplate.git
```

## Usage
Follow the following steps and you're good to go! 
Important: Typescript and npm has to be installed on your machine!

To install Typescript if missing - npm install -g typescript
I am not sure if "npm run lite" require Typescript, so I would go with running "npm run lite" first.

1: Clone repo
```
git clone https://maks_web_z@bitbucket.org/maks_web_z/vine-collection-app.git
```
2: Install packages
```
npm install
```
3: Start server (includes auto refreshing) and gulp watcher
```
npm run lite 
or
npm start
```
