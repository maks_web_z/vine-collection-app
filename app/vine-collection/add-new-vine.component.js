System.register(['angular2/core', "angular2/router", "../firebase.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, firebase_service_1;
    var AddNewVineComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (firebase_service_1_1) {
                firebase_service_1 = firebase_service_1_1;
            }],
        execute: function() {
            AddNewVineComponent = (function () {
                function AddNewVineComponent(_firebaseService, _router) {
                    this._firebaseService = _firebaseService;
                    this._router = _router;
                }
                AddNewVineComponent.prototype.onSubmit = function (form) {
                    var _this = this;
                    this._firebaseService.addNewVine(form.value.vineName, form.value.category, form.value.description, form.value.price, form.value.imageUrl)
                        .subscribe(function (response) {
                        _this.response = JSON.stringify(response);
                        _this._router.navigate(['VineList']);
                    }, function (error) { return console.log(error); });
                };
                AddNewVineComponent = __decorate([
                    core_1.Component({
                        selector: 'ann-new-vine',
                        template: "\n        <div class=\"add-new-vine-component\">\n            <div class=\"container\">\n                \n                <div class=\"row\">\n                    <div class=\"col-md-6\">\n                        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\n\n                            <div class=\"form-label\">Name of the vine</div>\n                            <input class=\"form-text-field\" type=\"text\" ngControl=\"vineName\" /> <br />\n\n                            <div class=\"form-label\">Category</div>\n                            <input class=\"form-text-field\" type=\"text\" ngControl=\"category\" /> <br />\n\n                            <div class=\"form-label\">Description</div>\n                            <textarea class=\"form-text-area description\" rows=\"7\" ngControl=\"description\"></textarea>\n\n                            <div class=\"form-label\">Price</div>\n                            <input class=\"form-text-field\" type=\"text\" ngControl=\"price\" /> <br />\n\n                            <div class=\"form-label\">Image Url as ./src/vine-images/cold-wind.png</div>\n                            <input class=\"form-text-field\" type=\"text\" ngControl=\"imageUrl\" /> <br />\n\n                            <button class=\"btn-submit\" type=\"submit\">Add new vine</button>\n                        </form>\n                    </div>                    \n                </div>                \n                \n            </div><!-- .container -->\n        </div><!-- .add-new-vine-component -->        \n    ",
                        providers: [firebase_service_1.FirebaseService]
                    }), 
                    __metadata('design:paramtypes', [firebase_service_1.FirebaseService, router_1.Router])
                ], AddNewVineComponent);
                return AddNewVineComponent;
            }());
            exports_1("AddNewVineComponent", AddNewVineComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZpbmUtY29sbGVjdGlvbi9hZGQtbmV3LXZpbmUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBeUNBO2dCQUlJLDZCQUFvQixnQkFBaUMsRUFBVSxPQUFlO29CQUExRCxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWlCO29CQUFVLFlBQU8sR0FBUCxPQUFPLENBQVE7Z0JBQUcsQ0FBQztnQkFFbEYsc0NBQVEsR0FBUixVQUFTLElBQWtCO29CQUEzQixpQkFTQztvQkFSRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO3lCQUNwSSxTQUFTLENBQ04sVUFBQSxRQUFRO3dCQUNKLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDekMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUN4QyxDQUFDLEVBQ0QsVUFBQSxLQUFLLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQixDQUM5QixDQUFDO2dCQUNWLENBQUM7Z0JBbkRMO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLGNBQWM7d0JBQ3hCLFFBQVEsRUFBRSwyaERBOEJUO3dCQUNELFNBQVMsRUFBRSxDQUFDLGtDQUFlLENBQUM7cUJBQy9CLENBQUM7O3VDQUFBO2dCQW1CRiwwQkFBQztZQUFELENBakJBLEFBaUJDLElBQUE7WUFqQkQscURBaUJDLENBQUEiLCJmaWxlIjoidmluZS1jb2xsZWN0aW9uL2FkZC1uZXctdmluZS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCB9IGZyb20gJ2FuZ3VsYXIyL2NvcmUnO1xyXG5pbXBvcnQge1JvdXRlcn0gZnJvbSBcImFuZ3VsYXIyL3JvdXRlclwiO1xyXG5pbXBvcnQge0NvbnRyb2xHcm91cH0gZnJvbSAnYW5ndWxhcjIvY29tbW9uJztcclxuaW1wb3J0IHtGaXJlYmFzZVNlcnZpY2V9IGZyb20gXCIuLi9maXJlYmFzZS5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYW5uLW5ldy12aW5lJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImFkZC1uZXctdmluZS1jb21wb25lbnRcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxmb3JtIChuZ1N1Ym1pdCk9XCJvblN1Ym1pdChmKVwiICNmPVwibmdGb3JtXCI+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tbGFiZWxcIj5OYW1lIG9mIHRoZSB2aW5lPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJmb3JtLXRleHQtZmllbGRcIiB0eXBlPVwidGV4dFwiIG5nQ29udHJvbD1cInZpbmVOYW1lXCIgLz4gPGJyIC8+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tbGFiZWxcIj5DYXRlZ29yeTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzPVwiZm9ybS10ZXh0LWZpZWxkXCIgdHlwZT1cInRleHRcIiBuZ0NvbnRyb2w9XCJjYXRlZ29yeVwiIC8+IDxiciAvPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWxhYmVsXCI+RGVzY3JpcHRpb248L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZXh0YXJlYSBjbGFzcz1cImZvcm0tdGV4dC1hcmVhIGRlc2NyaXB0aW9uXCIgcm93cz1cIjdcIiBuZ0NvbnRyb2w9XCJkZXNjcmlwdGlvblwiPjwvdGV4dGFyZWE+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tbGFiZWxcIj5QcmljZTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzPVwiZm9ybS10ZXh0LWZpZWxkXCIgdHlwZT1cInRleHRcIiBuZ0NvbnRyb2w9XCJwcmljZVwiIC8+IDxiciAvPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWxhYmVsXCI+SW1hZ2UgVXJsIGFzIC4vc3JjL3ZpbmUtaW1hZ2VzL2NvbGQtd2luZC5wbmc8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzcz1cImZvcm0tdGV4dC1maWVsZFwiIHR5cGU9XCJ0ZXh0XCIgbmdDb250cm9sPVwiaW1hZ2VVcmxcIiAvPiA8YnIgLz5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuLXN1Ym1pdFwiIHR5cGU9XCJzdWJtaXRcIj5BZGQgbmV3IHZpbmU8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICA8L2Rpdj4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgPC9kaXY+PCEtLSAuY29udGFpbmVyIC0tPlxyXG4gICAgICAgIDwvZGl2PjwhLS0gLmFkZC1uZXctdmluZS1jb21wb25lbnQgLS0+ICAgICAgICBcclxuICAgIGAsXHJcbiAgICBwcm92aWRlcnM6IFtGaXJlYmFzZVNlcnZpY2VdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQWRkTmV3VmluZUNvbXBvbmVudCB7XHJcblxyXG4gICAgcHVibGljIHJlc3BvbnNlOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfZmlyZWJhc2VTZXJ2aWNlOiBGaXJlYmFzZVNlcnZpY2UsIHByaXZhdGUgX3JvdXRlcjogUm91dGVyKSB7fVxyXG5cclxuICAgIG9uU3VibWl0KGZvcm06IENvbnRyb2xHcm91cCl7XHJcbiAgICAgICAgdGhpcy5fZmlyZWJhc2VTZXJ2aWNlLmFkZE5ld1ZpbmUoZm9ybS52YWx1ZS52aW5lTmFtZSwgZm9ybS52YWx1ZS5jYXRlZ29yeSwgZm9ybS52YWx1ZS5kZXNjcmlwdGlvbiwgZm9ybS52YWx1ZS5wcmljZSwgZm9ybS52YWx1ZS5pbWFnZVVybClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc3BvbnNlID0gSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbJ1ZpbmVMaXN0J10pO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxufSJdfQ==
