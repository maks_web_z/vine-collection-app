System.register(['angular2/core', "angular2/router", "../firebase.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, firebase_service_1;
    var VineDetailsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (firebase_service_1_1) {
                firebase_service_1 = firebase_service_1_1;
            }],
        execute: function() {
            VineDetailsComponent = (function () {
                function VineDetailsComponent(_routerParams, _firebaseService) {
                    this._routerParams = _routerParams;
                    this._firebaseService = _firebaseService;
                    this.showElement = false;
                }
                VineDetailsComponent.prototype.ngOnInit = function () {
                    this.vineId = this._routerParams.get('vineId');
                    this.getSingleVine();
                };
                VineDetailsComponent.prototype.getSingleVine = function () {
                    var _this = this;
                    this._firebaseService.getSingleVine(this.vineId)
                        .subscribe(function (response) {
                        _this.vineItem = response;
                        _this.showElement = true;
                    }, function (error) { return console.log(error); });
                };
                VineDetailsComponent = __decorate([
                    core_1.Component({
                        selector: 'vine-details',
                        template: "\n        <div class=\"vine-details-component\" *ngIf=\"showElement == true\">\n            <div class=\"container\">\n                 <div class=\"row\">\n                     \n                     <div class=\"col-md-4\">\n                         <div class=\"vine-img-wrapper\">\n                            <img class=\"vine-img img-responsive\" src=\"{{ vineItem.imageUrl }}\" />\n                         </div>\n                     </div>\n                     \n                     <div class=\"col-md-8\">\n                         \n                         <div class=\"vine-info-wrapper\">\n                             <div class=\"vine-price\">{{ vineItem.price }} kr.</div>\n                             <div class=\"vine-description\">{{ vineItem.description }}</div>\n                             <div class=\"details\">\n                                 Categories: <span class=\"text-red\">{{ vineItem.category }}</span>\n                             </div>\n                         </div>                         \n                         \n                     </div><!-- .col-md-8 -->\n                     \n                 </div><!-- .row -->\n            </div><!-- .container -->\n        </div><!-- .vine-details-component -->        \n    ",
                        providers: [firebase_service_1.FirebaseService]
                    }), 
                    __metadata('design:paramtypes', [router_1.RouteParams, firebase_service_1.FirebaseService])
                ], VineDetailsComponent);
                return VineDetailsComponent;
            }());
            exports_1("VineDetailsComponent", VineDetailsComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZpbmUtY29sbGVjdGlvbi92aW5lLWRldGFpbHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBb0NBO2dCQUVJLDhCQUFvQixhQUEwQixFQUFVLGdCQUFpQztvQkFBckUsa0JBQWEsR0FBYixhQUFhLENBQWE7b0JBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFpQjtvQkFJbEYsZ0JBQVcsR0FBRyxLQUFLLENBQUM7Z0JBSmlFLENBQUM7Z0JBTTdGLHVDQUFRLEdBQVI7b0JBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDL0MsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2dCQUN6QixDQUFDO2dCQUVELDRDQUFhLEdBQWI7b0JBQUEsaUJBU0M7b0JBUkcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO3lCQUMzQyxTQUFTLENBQ04sVUFBQSxRQUFRO3dCQUNKLEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO3dCQUN6QixLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztvQkFDNUIsQ0FBQyxFQUNELFVBQUEsS0FBSyxJQUFJLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBbEIsQ0FBa0IsQ0FDOUIsQ0FBQztnQkFDVixDQUFDO2dCQXRETDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxjQUFjO3dCQUN4QixRQUFRLEVBQUUsNnZDQTBCVDt3QkFDRCxTQUFTLEVBQUUsQ0FBQyxrQ0FBZSxDQUFDO3FCQUMvQixDQUFDOzt3Q0FBQTtnQkEwQkYsMkJBQUM7WUFBRCxDQXhCQSxBQXdCQyxJQUFBO1lBeEJELHVEQXdCQyxDQUFBIiwiZmlsZSI6InZpbmUtY29sbGVjdGlvbi92aW5lLWRldGFpbHMuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7Um91dGVQYXJhbXN9IGZyb20gXCJhbmd1bGFyMi9yb3V0ZXJcIjtcclxuaW1wb3J0IHtGaXJlYmFzZVNlcnZpY2V9IGZyb20gXCIuLi9maXJlYmFzZS5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAndmluZS1kZXRhaWxzJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInZpbmUtZGV0YWlscy1jb21wb25lbnRcIiAqbmdJZj1cInNob3dFbGVtZW50ID09IHRydWVcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInZpbmUtaW1nLXdyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgY2xhc3M9XCJ2aW5lLWltZyBpbWctcmVzcG9uc2l2ZVwiIHNyYz1cInt7IHZpbmVJdGVtLmltYWdlVXJsIH19XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtOFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidmluZS1pbmZvLXdyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidmluZS1wcmljZVwiPnt7IHZpbmVJdGVtLnByaWNlIH19IGtyLjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ2aW5lLWRlc2NyaXB0aW9uXCI+e3sgdmluZUl0ZW0uZGVzY3JpcHRpb24gfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZGV0YWlsc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBDYXRlZ29yaWVzOiA8c3BhbiBjbGFzcz1cInRleHQtcmVkXCI+e3sgdmluZUl0ZW0uY2F0ZWdvcnkgfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgPC9kaXY+PCEtLSAuY29sLW1kLTggLS0+XHJcbiAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgIDwvZGl2PjwhLS0gLnJvdyAtLT5cclxuICAgICAgICAgICAgPC9kaXY+PCEtLSAuY29udGFpbmVyIC0tPlxyXG4gICAgICAgIDwvZGl2PjwhLS0gLnZpbmUtZGV0YWlscy1jb21wb25lbnQgLS0+ICAgICAgICBcclxuICAgIGAsXHJcbiAgICBwcm92aWRlcnM6IFtGaXJlYmFzZVNlcnZpY2VdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVmluZURldGFpbHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3JvdXRlclBhcmFtczogUm91dGVQYXJhbXMsIHByaXZhdGUgX2ZpcmViYXNlU2VydmljZTogRmlyZWJhc2VTZXJ2aWNlKSB7fVxyXG5cclxuICAgIHB1YmxpYyB2aW5lSWQ6IHN0cmluZztcclxuICAgIHB1YmxpYyB2aW5lSXRlbToge307XHJcbiAgICBwdWJsaWMgc2hvd0VsZW1lbnQgPSBmYWxzZTtcclxuXHJcbiAgICBuZ09uSW5pdCgpOiBhbnkge1xyXG4gICAgICAgIHRoaXMudmluZUlkID0gdGhpcy5fcm91dGVyUGFyYW1zLmdldCgndmluZUlkJyk7XHJcbiAgICAgICAgdGhpcy5nZXRTaW5nbGVWaW5lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2luZ2xlVmluZSgpIHtcclxuICAgICAgICB0aGlzLl9maXJlYmFzZVNlcnZpY2UuZ2V0U2luZ2xlVmluZSh0aGlzLnZpbmVJZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnZpbmVJdGVtID0gcmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaG93RWxlbWVudCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG59Il19
