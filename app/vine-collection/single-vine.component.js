System.register(['angular2/core', "angular2/router"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1;
    var SingleVineComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            SingleVineComponent = (function () {
                function SingleVineComponent(_router) {
                    this._router = _router;
                }
                SingleVineComponent.prototype.showVineDetails = function () {
                    this._router.navigate(['VineDetails', { vineId: this.vineItem.key }]);
                };
                SingleVineComponent = __decorate([
                    core_1.Component({
                        selector: 'single-vine',
                        template: "\n        <section class=\"vine-item\">\n            <img class=\"vine-img img-responsive\" src=\"{{ vineItem.imageUrl }}\" />\n            <div class=\"vine-category\">{{ vineItem.category }}</div>\n            <div class=\"vine-name\">{{ vineItem.name }}</div>\n            <div class=\"price\">{{ vineItem.price }} kr.</div>\n            <button class=\"btn-details\" (click)=\"showVineDetails()\">Details</button>\n        </section>\n    ",
                        inputs: ['vineItem']
                    }), 
                    __metadata('design:paramtypes', [router_1.Router])
                ], SingleVineComponent);
                return SingleVineComponent;
            }());
            exports_1("SingleVineComponent", SingleVineComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZpbmUtY29sbGVjdGlvbi9zaW5nbGUtdmluZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFpQkE7Z0JBRUksNkJBQW9CLE9BQWU7b0JBQWYsWUFBTyxHQUFQLE9BQU8sQ0FBUTtnQkFBRyxDQUFDO2dCQUl2Qyw2Q0FBZSxHQUFmO29CQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsYUFBYSxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUMxRSxDQUFDO2dCQXRCTDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxhQUFhO3dCQUN2QixRQUFRLEVBQUUsNmJBUVQ7d0JBQ0QsTUFBTSxFQUFFLENBQUMsVUFBVSxDQUFDO3FCQUN2QixDQUFDOzt1Q0FBQTtnQkFXRiwwQkFBQztZQUFELENBVEEsQUFTQyxJQUFBO1lBVEQscURBU0MsQ0FBQSIsImZpbGUiOiJ2aW5lLWNvbGxlY3Rpb24vc2luZ2xlLXZpbmUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQgfSBmcm9tICdhbmd1bGFyMi9jb3JlJztcclxuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gXCJhbmd1bGFyMi9yb3V0ZXJcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdzaW5nbGUtdmluZScsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxzZWN0aW9uIGNsYXNzPVwidmluZS1pdGVtXCI+XHJcbiAgICAgICAgICAgIDxpbWcgY2xhc3M9XCJ2aW5lLWltZyBpbWctcmVzcG9uc2l2ZVwiIHNyYz1cInt7IHZpbmVJdGVtLmltYWdlVXJsIH19XCIgLz5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInZpbmUtY2F0ZWdvcnlcIj57eyB2aW5lSXRlbS5jYXRlZ29yeSB9fTwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwidmluZS1uYW1lXCI+e3sgdmluZUl0ZW0ubmFtZSB9fTwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicHJpY2VcIj57eyB2aW5lSXRlbS5wcmljZSB9fSBrci48L2Rpdj5cclxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0bi1kZXRhaWxzXCIgKGNsaWNrKT1cInNob3dWaW5lRGV0YWlscygpXCI+RGV0YWlsczwvYnV0dG9uPlxyXG4gICAgICAgIDwvc2VjdGlvbj5cclxuICAgIGAsXHJcbiAgICBpbnB1dHM6IFsndmluZUl0ZW0nXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNpbmdsZVZpbmVDb21wb25lbnQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3JvdXRlcjogUm91dGVyKSB7fVxyXG5cclxuICAgIHB1YmxpYyB2aW5lSXRlbToge307XHJcblxyXG4gICAgc2hvd1ZpbmVEZXRhaWxzKCl7XHJcbiAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlKFsnVmluZURldGFpbHMnLCB7IHZpbmVJZDogdGhpcy52aW5lSXRlbS5rZXkgfV0pO1xyXG4gICAgfVxyXG59Il19
