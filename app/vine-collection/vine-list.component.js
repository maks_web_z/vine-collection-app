System.register(['angular2/core', "./single-vine.component", "../firebase.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, single_vine_component_1, firebase_service_1;
    var VineListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (single_vine_component_1_1) {
                single_vine_component_1 = single_vine_component_1_1;
            },
            function (firebase_service_1_1) {
                firebase_service_1 = firebase_service_1_1;
            }],
        execute: function() {
            VineListComponent = (function () {
                function VineListComponent(_firebaseService) {
                    this._firebaseService = _firebaseService;
                    this.vineCollectionArray = [];
                    this.showList = false;
                }
                VineListComponent.prototype.ngOnInit = function () {
                    this.getVineCollection();
                };
                VineListComponent.prototype.getVineCollection = function () {
                    var _this = this;
                    this._firebaseService.getVines()
                        .subscribe(function (response) {
                        _this.vineCollectionObj = response;
                        _this.transformToArray(_this.vineCollectionObj);
                    }, function (error) { return console.log(error); });
                };
                VineListComponent.prototype.transformToArray = function (vineCollection) {
                    for (var key in vineCollection) {
                        this.vineCollectionArray.push({
                            'key': key,
                            'name': vineCollection[key].name,
                            'category': vineCollection[key].category,
                            'price': vineCollection[key].price,
                            'imageUrl': vineCollection[key].imageUrl,
                            'description': vineCollection[key].description
                        });
                    }
                    this.showList = true;
                };
                VineListComponent = __decorate([
                    core_1.Component({
                        selector: 'contact-list-component',
                        template: "        \n        <div class=\"view-list-component\">\n            \n            <div class=\"container\">\n                <div class=\"mini-heading\">Enjoy your</div>\n                <h1 class=\"heading\">Vine collection</h1>\n                <div class=\"row\">\n                    <div class=\"col-md-8 col-md-offset-2\">\n                        <div class=\"list-description\">\n                            Choose your favorite from a great variety of wines for every price point and any occasion, from rich\n                            shardonnay to elegant and creamy brut.\n                        </div>\n                    </div>\n                </div><!-- .row -->\n            </div><!-- .container -->\n\n            <div class=\"container\" *ngIf=\"showList == true\">\n                <div class=\"row\">\n                    \n                        <div *ngFor=\"#vineItem of vineCollectionArray\" class=\"col-md-3\">\n                            <single-vine [vineItem]=\"vineItem\"></single-vine>\n                        </div>\n\n                </div><!-- .row -->\n                \n                <div class=\"add-new-vine-wrapper\">\n                    <a href=\"/add-vine\" class=\"add-new-vine\">Add new vine</a>\n                </div>\n                \n            </div><!-- .container -->\n            \n        </div><!-- .view-list-component -->\n    ",
                        directives: [single_vine_component_1.SingleVineComponent],
                        providers: [firebase_service_1.FirebaseService]
                    }), 
                    __metadata('design:paramtypes', [firebase_service_1.FirebaseService])
                ], VineListComponent);
                return VineListComponent;
            }());
            exports_1("VineListComponent", VineListComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZpbmUtY29sbGVjdGlvbi92aW5lLWxpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBMkNBO2dCQU1JLDJCQUFvQixnQkFBaUM7b0JBQWpDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7b0JBSDlDLHdCQUFtQixHQUFHLEVBQUUsQ0FBQztvQkFDekIsYUFBUSxHQUFHLEtBQUssQ0FBQztnQkFFZ0MsQ0FBQztnQkFFekQsb0NBQVEsR0FBUjtvQkFDSSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztnQkFDN0IsQ0FBQztnQkFFRCw2Q0FBaUIsR0FBakI7b0JBQUEsaUJBU0M7b0JBUkcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRTt5QkFDM0IsU0FBUyxDQUNOLFVBQUEsUUFBUTt3QkFDSixLQUFJLENBQUMsaUJBQWlCLEdBQUcsUUFBUSxDQUFDO3dCQUNsQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7b0JBQ2xELENBQUMsRUFDRCxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQWxCLENBQWtCLENBQzlCLENBQUM7Z0JBQ1YsQ0FBQztnQkFFRCw0Q0FBZ0IsR0FBaEIsVUFBaUIsY0FBYztvQkFFM0IsR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQzt3QkFDN0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQzs0QkFDMUIsS0FBSyxFQUFFLEdBQUc7NEJBQ1YsTUFBTSxFQUFFLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJOzRCQUNoQyxVQUFVLEVBQUUsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVE7NEJBQ3hDLE9BQU8sRUFBRSxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSzs0QkFDbEMsVUFBVSxFQUFFLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFROzRCQUN4QyxhQUFhLEVBQUUsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQVc7eUJBQ2pELENBQUMsQ0FBQztvQkFDUCxDQUFDO29CQUVELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixDQUFDO2dCQTVFTDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSx3QkFBd0I7d0JBQ2xDLFFBQVEsRUFBRSxtM0NBZ0NUO3dCQUNELFVBQVUsRUFBRSxDQUFDLDJDQUFtQixDQUFDO3dCQUNqQyxTQUFTLEVBQUUsQ0FBQyxrQ0FBZSxDQUFDO3FCQUMvQixDQUFDOztxQ0FBQTtnQkF5Q0Ysd0JBQUM7WUFBRCxDQXZDQSxBQXVDQyxJQUFBO1lBdkNELGlEQXVDQyxDQUFBIiwiZmlsZSI6InZpbmUtY29sbGVjdGlvbi92aW5lLWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7U2luZ2xlVmluZUNvbXBvbmVudH0gZnJvbSBcIi4vc2luZ2xlLXZpbmUuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7RmlyZWJhc2VTZXJ2aWNlfSBmcm9tIFwiLi4vZmlyZWJhc2Uuc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NvbnRhY3QtbGlzdC1jb21wb25lbnQnLFxyXG4gICAgdGVtcGxhdGU6IGAgICAgICAgIFxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJ2aWV3LWxpc3QtY29tcG9uZW50XCI+XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibWluaS1oZWFkaW5nXCI+RW5qb3kgeW91cjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGgxIGNsYXNzPVwiaGVhZGluZ1wiPlZpbmUgY29sbGVjdGlvbjwvaDE+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC04IGNvbC1tZC1vZmZzZXQtMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGlzdC1kZXNjcmlwdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgQ2hvb3NlIHlvdXIgZmF2b3JpdGUgZnJvbSBhIGdyZWF0IHZhcmlldHkgb2Ygd2luZXMgZm9yIGV2ZXJ5IHByaWNlIHBvaW50IGFuZCBhbnkgb2NjYXNpb24sIGZyb20gcmljaFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hhcmRvbm5heSB0byBlbGVnYW50IGFuZCBjcmVhbXkgYnJ1dC5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj48IS0tIC5yb3cgLS0+XHJcbiAgICAgICAgICAgIDwvZGl2PjwhLS0gLmNvbnRhaW5lciAtLT5cclxuXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb250YWluZXJcIiAqbmdJZj1cInNob3dMaXN0ID09IHRydWVcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiAqbmdGb3I9XCIjdmluZUl0ZW0gb2YgdmluZUNvbGxlY3Rpb25BcnJheVwiIGNsYXNzPVwiY29sLW1kLTNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzaW5nbGUtdmluZSBbdmluZUl0ZW1dPVwidmluZUl0ZW1cIj48L3NpbmdsZS12aW5lPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8L2Rpdj48IS0tIC5yb3cgLS0+XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhZGQtbmV3LXZpbmUtd3JhcHBlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIvYWRkLXZpbmVcIiBjbGFzcz1cImFkZC1uZXctdmluZVwiPkFkZCBuZXcgdmluZTwvYT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIDwvZGl2PjwhLS0gLmNvbnRhaW5lciAtLT5cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgPC9kaXY+PCEtLSAudmlldy1saXN0LWNvbXBvbmVudCAtLT5cclxuICAgIGAsXHJcbiAgICBkaXJlY3RpdmVzOiBbU2luZ2xlVmluZUNvbXBvbmVudF0sXHJcbiAgICBwcm92aWRlcnM6IFtGaXJlYmFzZVNlcnZpY2VdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVmluZUxpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHB1YmxpYyB2aW5lQ29sbGVjdGlvbk9iajogYW55O1xyXG4gICAgcHVibGljIHZpbmVDb2xsZWN0aW9uQXJyYXkgPSBbXTtcclxuICAgIHB1YmxpYyBzaG93TGlzdCA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2ZpcmViYXNlU2VydmljZTogRmlyZWJhc2VTZXJ2aWNlKSB7fVxyXG5cclxuICAgIG5nT25Jbml0KCk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5nZXRWaW5lQ29sbGVjdGlvbigpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFZpbmVDb2xsZWN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX2ZpcmViYXNlU2VydmljZS5nZXRWaW5lcygpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aW5lQ29sbGVjdGlvbk9iaiA9IHJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHJhbnNmb3JtVG9BcnJheSh0aGlzLnZpbmVDb2xsZWN0aW9uT2JqKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcilcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICB0cmFuc2Zvcm1Ub0FycmF5KHZpbmVDb2xsZWN0aW9uKXtcclxuXHJcbiAgICAgICAgZm9yICh2YXIga2V5IGluIHZpbmVDb2xsZWN0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMudmluZUNvbGxlY3Rpb25BcnJheS5wdXNoKHtcclxuICAgICAgICAgICAgICAgICdrZXknOiBrZXksXHJcbiAgICAgICAgICAgICAgICAnbmFtZSc6IHZpbmVDb2xsZWN0aW9uW2tleV0ubmFtZSxcclxuICAgICAgICAgICAgICAgICdjYXRlZ29yeSc6IHZpbmVDb2xsZWN0aW9uW2tleV0uY2F0ZWdvcnksXHJcbiAgICAgICAgICAgICAgICAncHJpY2UnOiB2aW5lQ29sbGVjdGlvbltrZXldLnByaWNlLFxyXG4gICAgICAgICAgICAgICAgJ2ltYWdlVXJsJzogdmluZUNvbGxlY3Rpb25ba2V5XS5pbWFnZVVybCxcclxuICAgICAgICAgICAgICAgICdkZXNjcmlwdGlvbic6IHZpbmVDb2xsZWN0aW9uW2tleV0uZGVzY3JpcHRpb25cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNob3dMaXN0ID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbn0iXX0=
