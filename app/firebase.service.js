System.register(['angular2/core', 'angular2/http', 'rxjs/Rx'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1;
    var FirebaseService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            FirebaseService = (function () {
                function FirebaseService(_http) {
                    this._http = _http;
                }
                FirebaseService.prototype.getVines = function () {
                    return this._http.get('https://contact-app-a1b14.firebaseio.com/vine-collection.json')
                        .map(function (response) { return response.json(); });
                };
                FirebaseService.prototype.getSingleVine = function (key) {
                    return this._http.get('https://contact-app-a1b14.firebaseio.com/vine-collection/' + key + '.json')
                        .map(function (response) { return response.json(); });
                };
                FirebaseService.prototype.addNewVine = function (vineName, category, description, price, imageUrl) {
                    var body = JSON.stringify({
                        name: vineName,
                        category: category,
                        description: description,
                        price: price,
                        imageUrl: imageUrl
                    });
                    console.log(body);
                    return this._http.post('https://contact-app-a1b14.firebaseio.com/vine-collection.json', body)
                        .map(function (response) { return response.json(); });
                };
                FirebaseService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], FirebaseService);
                return FirebaseService;
            }());
            exports_1("FirebaseService", FirebaseService);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZpcmViYXNlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBS0E7Z0JBRUkseUJBQW9CLEtBQVc7b0JBQVgsVUFBSyxHQUFMLEtBQUssQ0FBTTtnQkFBRyxDQUFDO2dCQUVuQyxrQ0FBUSxHQUFSO29CQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQywrREFBK0QsQ0FBQzt5QkFDakYsR0FBRyxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsUUFBUSxDQUFDLElBQUksRUFBRSxFQUFmLENBQWUsQ0FBQyxDQUFDO2dCQUMxQyxDQUFDO2dCQUVELHVDQUFhLEdBQWIsVUFBYyxHQUFXO29CQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsMkRBQTJELEdBQUcsR0FBRyxHQUFJLE9BQU8sQ0FBQzt5QkFDOUYsR0FBRyxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsUUFBUSxDQUFDLElBQUksRUFBRSxFQUFmLENBQWUsQ0FBQyxDQUFDO2dCQUMxQyxDQUFDO2dCQUVELG9DQUFVLEdBQVYsVUFBVyxRQUFnQixFQUFFLFFBQWdCLEVBQUUsV0FBbUIsRUFBRSxLQUFhLEVBQUUsUUFBZ0I7b0JBQy9GLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7d0JBQ3hCLElBQUksRUFBRSxRQUFRO3dCQUNkLFFBQVEsRUFBRSxRQUFRO3dCQUNsQixXQUFXLEVBQUUsV0FBVzt3QkFDeEIsS0FBSyxFQUFFLEtBQUs7d0JBQ1osUUFBUSxFQUFFLFFBQVE7cUJBQ3JCLENBQUMsQ0FBQztvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUVsQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsK0RBQStELEVBQUUsSUFBSSxDQUFDO3lCQUN4RixHQUFHLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsSUFBSSxFQUFFLEVBQWYsQ0FBZSxDQUFDLENBQUM7Z0JBQzFDLENBQUM7Z0JBM0JMO29CQUFDLGlCQUFVLEVBQUU7O21DQUFBO2dCQTZCYixzQkFBQztZQUFELENBNUJBLEFBNEJDLElBQUE7WUE1QkQsNkNBNEJDLENBQUEiLCJmaWxlIjoiZmlyZWJhc2Uuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnYW5ndWxhcjIvY29yZSdcclxuaW1wb3J0IHtIdHRwfSBmcm9tICdhbmd1bGFyMi9odHRwJ1xyXG5pbXBvcnQgJ3J4anMvUngnXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBGaXJlYmFzZVNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2h0dHA6IEh0dHApIHt9XHJcblxyXG4gICAgZ2V0VmluZXMoKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5nZXQoJ2h0dHBzOi8vY29udGFjdC1hcHAtYTFiMTQuZmlyZWJhc2Vpby5jb20vdmluZS1jb2xsZWN0aW9uLmpzb24nKVxyXG4gICAgICAgICAgICAubWFwKHJlc3BvbnNlID0+IHJlc3BvbnNlLmpzb24oKSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2luZ2xlVmluZShrZXk6IHN0cmluZyl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAuZ2V0KCdodHRwczovL2NvbnRhY3QtYXBwLWExYjE0LmZpcmViYXNlaW8uY29tL3ZpbmUtY29sbGVjdGlvbi8nICsga2V5ICsgICcuanNvbicpXHJcbiAgICAgICAgICAgIC5tYXAocmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpKTtcclxuICAgIH1cclxuXHJcbiAgICBhZGROZXdWaW5lKHZpbmVOYW1lOiBzdHJpbmcsIGNhdGVnb3J5OiBzdHJpbmcsIGRlc2NyaXB0aW9uOiBzdHJpbmcsIHByaWNlOiBzdHJpbmcsIGltYWdlVXJsOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgY29uc3QgYm9keSA9IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICAgICAgbmFtZTogdmluZU5hbWUsXHJcbiAgICAgICAgICAgIGNhdGVnb3J5OiBjYXRlZ29yeSxcclxuICAgICAgICAgICAgZGVzY3JpcHRpb246IGRlc2NyaXB0aW9uLFxyXG4gICAgICAgICAgICBwcmljZTogcHJpY2UsXHJcbiAgICAgICAgICAgIGltYWdlVXJsOiBpbWFnZVVybFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGJvZHkpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5wb3N0KCdodHRwczovL2NvbnRhY3QtYXBwLWExYjE0LmZpcmViYXNlaW8uY29tL3ZpbmUtY29sbGVjdGlvbi5qc29uJywgYm9keSlcclxuICAgICAgICAgICAgLm1hcChyZXNwb25zZSA9PiByZXNwb25zZS5qc29uKCkpO1xyXG4gICAgfVxyXG5cclxufSJdfQ==
