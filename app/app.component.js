System.register(['angular2/core', "angular2/router", "./vine-collection/vine-list.component", "./vine-collection/vine-details.component", "./vine-collection/add-new-vine.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, vine_list_component_1, vine_details_component_1, add_new_vine_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (vine_list_component_1_1) {
                vine_list_component_1 = vine_list_component_1_1;
            },
            function (vine_details_component_1_1) {
                vine_details_component_1 = vine_details_component_1_1;
            },
            function (add_new_vine_component_1_1) {
                add_new_vine_component_1 = add_new_vine_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                }
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "\n        <header>\n            <div class=\"container\">\n                <div class=\"img-background\"></div>\n            </div>\n        </header>        \n        <main>\n            <router-outlet></router-outlet>\n        </main>\n    ",
                        directives: [vine_list_component_1.VineListComponent, vine_details_component_1.VineDetailsComponent, add_new_vine_component_1.AddNewVineComponent, router_1.ROUTER_DIRECTIVES]
                    }),
                    router_1.RouteConfig([
                        { path: '/vine-list', name: 'VineList', component: vine_list_component_1.VineListComponent, useAsDefault: true },
                        { path: '/vine-details/:vineId', name: 'VineDetails', component: vine_details_component_1.VineDetailsComponent },
                        { path: '/add-vine', name: 'AddNewVine', component: add_new_vine_component_1.AddNewVineComponent },
                    ]), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUEyQkE7Z0JBQUE7Z0JBR0EsQ0FBQztnQkF4QkQ7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsUUFBUTt3QkFDbEIsUUFBUSxFQUFFLG9QQVNUO3dCQUNELFVBQVUsRUFBRSxDQUFDLHVDQUFpQixFQUFFLDZDQUFvQixFQUFFLDRDQUFtQixFQUFFLDBCQUFpQixDQUFDO3FCQUNoRyxDQUFDO29CQUVELG9CQUFXLENBQUM7d0JBQ1QsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLHVDQUFpQixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7d0JBQ3pGLEVBQUUsSUFBSSxFQUFFLHVCQUF1QixFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLDZDQUFvQixFQUFDO3dCQUN0RixFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsNENBQW1CLEVBQUM7cUJBQzNFLENBQUM7O2dDQUFBO2dCQUtGLG1CQUFDO1lBQUQsQ0FIQSxBQUdDLElBQUE7WUFIRCx1Q0FHQyxDQUFBIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XG5pbXBvcnQge1JvdXRlQ29uZmlnLCBST1VURVJfRElSRUNUSVZFU30gZnJvbSBcImFuZ3VsYXIyL3JvdXRlclwiO1xuaW1wb3J0IHtWaW5lTGlzdENvbXBvbmVudH0gZnJvbSBcIi4vdmluZS1jb2xsZWN0aW9uL3ZpbmUtbGlzdC5jb21wb25lbnRcIjtcbmltcG9ydCB7VmluZURldGFpbHNDb21wb25lbnR9IGZyb20gXCIuL3ZpbmUtY29sbGVjdGlvbi92aW5lLWRldGFpbHMuY29tcG9uZW50XCI7XG5pbXBvcnQge0FkZE5ld1ZpbmVDb21wb25lbnR9IGZyb20gXCIuL3ZpbmUtY29sbGVjdGlvbi9hZGQtbmV3LXZpbmUuY29tcG9uZW50XCI7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnbXktYXBwJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8aGVhZGVyPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbWctYmFja2dyb3VuZFwiPjwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvaGVhZGVyPiAgICAgICAgXG4gICAgICAgIDxtYWluPlxuICAgICAgICAgICAgPHJvdXRlci1vdXRsZXQ+PC9yb3V0ZXItb3V0bGV0PlxuICAgICAgICA8L21haW4+XG4gICAgYCxcbiAgICBkaXJlY3RpdmVzOiBbVmluZUxpc3RDb21wb25lbnQsIFZpbmVEZXRhaWxzQ29tcG9uZW50LCBBZGROZXdWaW5lQ29tcG9uZW50LCBST1VURVJfRElSRUNUSVZFU11cbn0pXG5cbkBSb3V0ZUNvbmZpZyhbXG4gICAgeyBwYXRoOiAnL3ZpbmUtbGlzdCcsIG5hbWU6ICdWaW5lTGlzdCcsIGNvbXBvbmVudDogVmluZUxpc3RDb21wb25lbnQsIHVzZUFzRGVmYXVsdDogdHJ1ZX0sXG4gICAgeyBwYXRoOiAnL3ZpbmUtZGV0YWlscy86dmluZUlkJywgbmFtZTogJ1ZpbmVEZXRhaWxzJywgY29tcG9uZW50OiBWaW5lRGV0YWlsc0NvbXBvbmVudH0sXG4gICAgeyBwYXRoOiAnL2FkZC12aW5lJywgbmFtZTogJ0FkZE5ld1ZpbmUnLCBjb21wb25lbnQ6IEFkZE5ld1ZpbmVDb21wb25lbnR9LFxuXSlcblxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCB7XG4gICAgbmFtZTogc3RyaW5nO1xuXG59Il19
