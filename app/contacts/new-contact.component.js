System.register(['angular2/core', "../firebase.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, firebase_service_1;
    var NewContactComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (firebase_service_1_1) {
                firebase_service_1 = firebase_service_1_1;
            }],
        execute: function() {
            NewContactComponent = (function () {
                function NewContactComponent(_firebaseService) {
                    this._firebaseService = _firebaseService;
                }
                NewContactComponent.prototype.onSubmit = function (form) {
                    var _this = this;
                    this._firebaseService.setUser(form.value.firstName, form.value.lastName)
                        .subscribe(function (user) { return _this.response = JSON.stringify(user); }, function (error) { return console.log(error); });
                };
                NewContactComponent.prototype.onGetUser = function () {
                    var _this = this;
                    this._firebaseService.getUser()
                        .subscribe(function (user) { return _this.response = JSON.stringify(user); }, function (error) { return console.log(error); });
                };
                NewContactComponent = __decorate([
                    core_1.Component({
                        selector: 'new-contact-component',
                        template: "\n        <h4>Create new Contact</h4>\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\n            First name: <br />\n            <input type=\"text\" ngControl=\"firstName\" /> <br />\n            Last name: <br />\n            <input type=\"text\" ngControl=\"lastName\" /> <br />\n            Phone Number: <br />\n            <input type=\"text\" /> <br />\n            Email: <br />\n            <input type=\"text\" /> <br />\n            <button type=\"submit\">Create Contact</button>\n        </form>\n        <br />\n        <br />\n        <br />\n        <div id=\"get-user\" class=\"container\">\n            <h2>Get user</h2>\n            <button (click)=\"onGetUser()\">Get User</button>\n        </div>\n        <br />\n        <br />\n        <div id=\"response\">\n            Response: {{ response }}\n        </div>\n    ",
                        providers: [firebase_service_1.FirebaseService]
                    }), 
                    __metadata('design:paramtypes', [firebase_service_1.FirebaseService])
                ], NewContactComponent);
                return NewContactComponent;
            }());
            exports_1("NewContactComponent", NewContactComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRhY3RzL25ldy1jb250YWN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQW1DQTtnQkFHSSw2QkFBb0IsZ0JBQWlDO29CQUFqQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWlCO2dCQUFHLENBQUM7Z0JBRXpELHNDQUFRLEdBQVIsVUFBUyxJQUFrQjtvQkFBM0IsaUJBTUM7b0JBTEcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQzt5QkFDbkUsU0FBUyxDQUNOLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFwQyxDQUFvQyxFQUM1QyxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQWxCLENBQWtCLENBQzlCLENBQUM7Z0JBQ1YsQ0FBQztnQkFFRCx1Q0FBUyxHQUFUO29CQUFBLGlCQU1DO29CQUxHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7eUJBQzFCLFNBQVMsQ0FDTixVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBcEMsQ0FBb0MsRUFDNUMsVUFBQSxLQUFLLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQixDQUM5QixDQUFDO2dCQUNWLENBQUM7Z0JBbERMO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLHVCQUF1Qjt3QkFDakMsUUFBUSxFQUFFLHExQkF5QlQ7d0JBQ0QsU0FBUyxFQUFFLENBQUMsa0NBQWUsQ0FBQztxQkFDL0IsQ0FBQzs7dUNBQUE7Z0JBdUJGLDBCQUFDO1lBQUQsQ0FyQkEsQUFxQkMsSUFBQTtZQXJCRCxxREFxQkMsQ0FBQSIsImZpbGUiOiJjb250YWN0cy9uZXctY29udGFjdC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7Q29udHJvbEdyb3VwfSBmcm9tICdhbmd1bGFyMi9jb21tb24nO1xyXG5pbXBvcnQge0ZpcmViYXNlU2VydmljZX0gZnJvbSBcIi4uL2ZpcmViYXNlLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICduZXctY29udGFjdC1jb21wb25lbnQnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8aDQ+Q3JlYXRlIG5ldyBDb250YWN0PC9oND5cclxuICAgICAgICA8Zm9ybSAobmdTdWJtaXQpPVwib25TdWJtaXQoZilcIiAjZj1cIm5nRm9ybVwiPlxyXG4gICAgICAgICAgICBGaXJzdCBuYW1lOiA8YnIgLz5cclxuICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgbmdDb250cm9sPVwiZmlyc3ROYW1lXCIgLz4gPGJyIC8+XHJcbiAgICAgICAgICAgIExhc3QgbmFtZTogPGJyIC8+XHJcbiAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIG5nQ29udHJvbD1cImxhc3ROYW1lXCIgLz4gPGJyIC8+XHJcbiAgICAgICAgICAgIFBob25lIE51bWJlcjogPGJyIC8+XHJcbiAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIC8+IDxiciAvPlxyXG4gICAgICAgICAgICBFbWFpbDogPGJyIC8+XHJcbiAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIC8+IDxiciAvPlxyXG4gICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIj5DcmVhdGUgQ29udGFjdDwvYnV0dG9uPlxyXG4gICAgICAgIDwvZm9ybT5cclxuICAgICAgICA8YnIgLz5cclxuICAgICAgICA8YnIgLz5cclxuICAgICAgICA8YnIgLz5cclxuICAgICAgICA8ZGl2IGlkPVwiZ2V0LXVzZXJcIiBjbGFzcz1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICA8aDI+R2V0IHVzZXI8L2gyPlxyXG4gICAgICAgICAgICA8YnV0dG9uIChjbGljayk9XCJvbkdldFVzZXIoKVwiPkdldCBVc2VyPC9idXR0b24+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgPGRpdiBpZD1cInJlc3BvbnNlXCI+XHJcbiAgICAgICAgICAgIFJlc3BvbnNlOiB7eyByZXNwb25zZSB9fVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgYCxcclxuICAgIHByb3ZpZGVyczogW0ZpcmViYXNlU2VydmljZV1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBOZXdDb250YWN0Q29tcG9uZW50IHtcclxuICAgIHB1YmxpYyByZXNwb25zZTogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2ZpcmViYXNlU2VydmljZTogRmlyZWJhc2VTZXJ2aWNlKSB7fVxyXG5cclxuICAgIG9uU3VibWl0KGZvcm06IENvbnRyb2xHcm91cCl7XHJcbiAgICAgICAgdGhpcy5fZmlyZWJhc2VTZXJ2aWNlLnNldFVzZXIoZm9ybS52YWx1ZS5maXJzdE5hbWUsIGZvcm0udmFsdWUubGFzdE5hbWUpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICB1c2VyID0+IHRoaXMucmVzcG9uc2UgPSBKU09OLnN0cmluZ2lmeSh1c2VyKSxcclxuICAgICAgICAgICAgICAgIGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIG9uR2V0VXNlcigpIHtcclxuICAgICAgICB0aGlzLl9maXJlYmFzZVNlcnZpY2UuZ2V0VXNlcigpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICB1c2VyID0+IHRoaXMucmVzcG9uc2UgPSBKU09OLnN0cmluZ2lmeSh1c2VyKSxcclxuICAgICAgICAgICAgICAgIGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxufSJdfQ==
