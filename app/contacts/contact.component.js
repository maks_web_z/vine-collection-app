System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var ContactComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            ContactComponent = (function () {
                function ContactComponent() {
                    this.contact = {};
                }
                ContactComponent = __decorate([
                    core_1.Component({
                        selector: 'contact-component',
                        template: "\n        <h4>Contact details:</h4>\n        <div>\n            First name: <br />\n            <input [(ngModel)]=\"contact.firstName\" type=\"text\" /> <br />\n            Last name: <br />\n            <input [(ngModel)]=\"contact.lastName\" type=\"text\" /> <br />\n            Phone Number: <br />\n            <input [(ngModel)]=\"contact.phone\" type=\"text\" /> <br />\n            Email: <br />\n            <input [(ngModel)]=\"contact.email\" type=\"text\" /> <br />\n        </div>\n    ",
                        inputs: ['contact']
                    }), 
                    __metadata('design:paramtypes', [])
                ], ContactComponent);
                return ContactComponent;
            }());
            exports_1("ContactComponent", ContactComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRhY3RzL2NvbnRhY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBb0JBO2dCQUFBO29CQUNXLFlBQU8sR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLENBQUM7Z0JBcEJEO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLG1CQUFtQjt3QkFDN0IsUUFBUSxFQUFFLHFmQVlUO3dCQUNELE1BQU0sRUFBRSxDQUFDLFNBQVMsQ0FBQztxQkFDdEIsQ0FBQzs7b0NBQUE7Z0JBSUYsdUJBQUM7WUFBRCxDQUZBLEFBRUMsSUFBQTtZQUZELCtDQUVDLENBQUEiLCJmaWxlIjoiY29udGFjdHMvY29udGFjdC5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY29udGFjdC1jb21wb25lbnQnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8aDQ+Q29udGFjdCBkZXRhaWxzOjwvaDQ+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgRmlyc3QgbmFtZTogPGJyIC8+XHJcbiAgICAgICAgICAgIDxpbnB1dCBbKG5nTW9kZWwpXT1cImNvbnRhY3QuZmlyc3ROYW1lXCIgdHlwZT1cInRleHRcIiAvPiA8YnIgLz5cclxuICAgICAgICAgICAgTGFzdCBuYW1lOiA8YnIgLz5cclxuICAgICAgICAgICAgPGlucHV0IFsobmdNb2RlbCldPVwiY29udGFjdC5sYXN0TmFtZVwiIHR5cGU9XCJ0ZXh0XCIgLz4gPGJyIC8+XHJcbiAgICAgICAgICAgIFBob25lIE51bWJlcjogPGJyIC8+XHJcbiAgICAgICAgICAgIDxpbnB1dCBbKG5nTW9kZWwpXT1cImNvbnRhY3QucGhvbmVcIiB0eXBlPVwidGV4dFwiIC8+IDxiciAvPlxyXG4gICAgICAgICAgICBFbWFpbDogPGJyIC8+XHJcbiAgICAgICAgICAgIDxpbnB1dCBbKG5nTW9kZWwpXT1cImNvbnRhY3QuZW1haWxcIiB0eXBlPVwidGV4dFwiIC8+IDxiciAvPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgYCxcclxuICAgIGlucHV0czogWydjb250YWN0J11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDb250YWN0Q29tcG9uZW50IHtcclxuICAgIHB1YmxpYyBjb250YWN0ID0ge307XHJcbn0iXX0=
