System.register(['angular2/core', "./contact.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, contact_component_1;
    var ContactListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (contact_component_1_1) {
                contact_component_1 = contact_component_1_1;
            }],
        execute: function() {
            ContactListComponent = (function () {
                function ContactListComponent() {
                    this.contacts = [];
                    this.selectedContact = {};
                    this.contacts = [
                        {
                            firstName: 'Max',
                            lastName: 'Zhovty',
                            email: 'maximzov@hotmail.com',
                            phone: '+45 5033 4783'
                        },
                        {
                            firstName: 'Bill',
                            lastName: 'Ivanov',
                            email: 'bill@hotmail.com',
                            phone: '+45 1122 1122'
                        },
                        {
                            firstName: 'Dmitriy',
                            lastName: 'Banan',
                            email: 'andy@hotmail.com',
                            phone: '+45 3344 3344'
                        }
                    ];
                }
                ContactListComponent.prototype.onSelect = function (contact) {
                    this.selectedContact = contact;
                    console.log('on select');
                };
                ContactListComponent = __decorate([
                    core_1.Component({
                        selector: 'contact-list-component',
                        template: "\n        <h3>Contact list component</h3>\n        <ul *ngFor=\"#contact of contacts\">\n            <li (click)=\"onSelect(contact)\"\n                [class.clicked]=\"selectedContact == contact\"\n            >\n                {{contact.firstName}} {{contact.lastName}}\n            </li>\n        </ul>\n        \n        <contact-component [contact]=\"selectedContact\"></contact-component>\n    ",
                        directives: [contact_component_1.ContactComponent],
                        inputs: ['contacts']
                    }), 
                    __metadata('design:paramtypes', [])
                ], ContactListComponent);
                return ContactListComponent;
            }());
            exports_1("ContactListComponent", ContactListComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRhY3RzL2NvbnRhY3QtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFxQkE7Z0JBQUE7b0JBRVcsYUFBUSxHQUFHLEVBQUUsQ0FBQztvQkFDZCxvQkFBZSxHQUFHLEVBQUUsQ0FBQztvQkFPckIsYUFBUSxHQUFHO3dCQUNkOzRCQUNJLFNBQVMsRUFBRSxLQUFLOzRCQUNoQixRQUFRLEVBQUUsUUFBUTs0QkFDbEIsS0FBSyxFQUFFLHNCQUFzQjs0QkFDN0IsS0FBSyxFQUFFLGVBQWU7eUJBQ3pCO3dCQUNEOzRCQUNJLFNBQVMsRUFBRSxNQUFNOzRCQUNqQixRQUFRLEVBQUUsUUFBUTs0QkFDbEIsS0FBSyxFQUFFLGtCQUFrQjs0QkFDekIsS0FBSyxFQUFFLGVBQWU7eUJBQ3pCO3dCQUNEOzRCQUNJLFNBQVMsRUFBRSxTQUFTOzRCQUNwQixRQUFRLEVBQUUsT0FBTzs0QkFDakIsS0FBSyxFQUFFLGtCQUFrQjs0QkFDekIsS0FBSyxFQUFFLGVBQWU7eUJBQ3pCO3FCQUNKLENBQUM7Z0JBRU4sQ0FBQztnQkExQkcsdUNBQVEsR0FBUixVQUFTLE9BQU87b0JBQ1osSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUM7b0JBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzdCLENBQUM7Z0JBMUJMO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLHdCQUF3Qjt3QkFDbEMsUUFBUSxFQUFFLG9aQVdUO3dCQUNELFVBQVUsRUFBRSxDQUFDLG9DQUFnQixDQUFDO3dCQUM5QixNQUFNLEVBQUUsQ0FBQyxVQUFVLENBQUM7cUJBQ3ZCLENBQUM7O3dDQUFBO2dCQWlDRiwyQkFBQztZQUFELENBL0JBLEFBK0JDLElBQUE7WUEvQkQsdURBK0JDLENBQUEiLCJmaWxlIjoiY29udGFjdHMvY29udGFjdC1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ2FuZ3VsYXIyL2NvcmUnO1xyXG5pbXBvcnQge0NvbnRhY3RDb21wb25lbnR9IGZyb20gXCIuL2NvbnRhY3QuY29tcG9uZW50XCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY29udGFjdC1saXN0LWNvbXBvbmVudCcsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxoMz5Db250YWN0IGxpc3QgY29tcG9uZW50PC9oMz5cclxuICAgICAgICA8dWwgKm5nRm9yPVwiI2NvbnRhY3Qgb2YgY29udGFjdHNcIj5cclxuICAgICAgICAgICAgPGxpIChjbGljayk9XCJvblNlbGVjdChjb250YWN0KVwiXHJcbiAgICAgICAgICAgICAgICBbY2xhc3MuY2xpY2tlZF09XCJzZWxlY3RlZENvbnRhY3QgPT0gY29udGFjdFwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHt7Y29udGFjdC5maXJzdE5hbWV9fSB7e2NvbnRhY3QubGFzdE5hbWV9fVxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgIDwvdWw+XHJcbiAgICAgICAgXHJcbiAgICAgICAgPGNvbnRhY3QtY29tcG9uZW50IFtjb250YWN0XT1cInNlbGVjdGVkQ29udGFjdFwiPjwvY29udGFjdC1jb21wb25lbnQ+XHJcbiAgICBgLFxyXG4gICAgZGlyZWN0aXZlczogW0NvbnRhY3RDb21wb25lbnRdLFxyXG4gICAgaW5wdXRzOiBbJ2NvbnRhY3RzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDb250YWN0TGlzdENvbXBvbmVudCB7XHJcblxyXG4gICAgcHVibGljIGNvbnRhY3RzID0gW107XHJcbiAgICBwdWJsaWMgc2VsZWN0ZWRDb250YWN0ID0ge307XHJcblxyXG4gICAgb25TZWxlY3QoY29udGFjdCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRDb250YWN0ID0gY29udGFjdDtcclxuICAgICAgICBjb25zb2xlLmxvZygnb24gc2VsZWN0Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNvbnRhY3RzID0gW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmlyc3ROYW1lOiAnTWF4JyxcclxuICAgICAgICAgICAgbGFzdE5hbWU6ICdaaG92dHknLFxyXG4gICAgICAgICAgICBlbWFpbDogJ21heGltem92QGhvdG1haWwuY29tJyxcclxuICAgICAgICAgICAgcGhvbmU6ICcrNDUgNTAzMyA0NzgzJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBmaXJzdE5hbWU6ICdCaWxsJyxcclxuICAgICAgICAgICAgbGFzdE5hbWU6ICdJdmFub3YnLFxyXG4gICAgICAgICAgICBlbWFpbDogJ2JpbGxAaG90bWFpbC5jb20nLFxyXG4gICAgICAgICAgICBwaG9uZTogJys0NSAxMTIyIDExMjInXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpcnN0TmFtZTogJ0RtaXRyaXknLFxyXG4gICAgICAgICAgICBsYXN0TmFtZTogJ0JhbmFuJyxcclxuICAgICAgICAgICAgZW1haWw6ICdhbmR5QGhvdG1haWwuY29tJyxcclxuICAgICAgICAgICAgcGhvbmU6ICcrNDUgMzM0NCAzMzQ0J1xyXG4gICAgICAgIH1cclxuICAgIF07XHJcblxyXG59Il19
