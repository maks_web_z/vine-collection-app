System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var ContactListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            ContactListComponent = (function () {
                function ContactListComponent() {
                    this.contacts = [
                        {
                            firstName: 'Max',
                            lastName: 'Zhovty',
                            email: 'maximzov@hotmail.com',
                            phone: '+45 5033 4783'
                        },
                        {
                            firstName: 'Bill',
                            lastName: 'Ivanov',
                            email: 'bill@hotmail.com',
                            phone: '+45 1122 1122'
                        },
                        {
                            firstName: 'Dmitriy',
                            lastName: 'Banan',
                            email: 'andy@hotmail.com',
                            phone: '+45 3344 3344'
                        }
                    ];
                    this.selectedContact = {};
                }
                ContactListComponent.prototype.onSelect = function (contact) {
                    this.selectedContact = contact;
                    console.log('on select');
                };
                ContactListComponent = __decorate([
                    core_1.Component({
                        selector: 'contact-list-component',
                        template: "\n        <h3>Cotnact list component</h3>\n        <ul *ngFor=\"#contact of contacts\">\n            <li (click)=\"onSelect(contact)\"\n                [class.clicked]=\"showDetail == true\"\n            >\n                {{contact.firstName}} {{contact.lastName}}\n            </li>\n        </ul>\n    ",
                    }), 
                    __metadata('design:paramtypes', [])
                ], ContactListComponent);
                return ContactListComponent;
            }());
            exports_1("ContactListComponent", ContactListComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlbXBsYXRlcy9jb250YWN0cy9jb250YWN0LWxpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBZ0JBO2dCQUFBO29CQUNXLGFBQVEsR0FBRzt3QkFDZDs0QkFDSSxTQUFTLEVBQUUsS0FBSzs0QkFDaEIsUUFBUSxFQUFFLFFBQVE7NEJBQ2xCLEtBQUssRUFBRSxzQkFBc0I7NEJBQzdCLEtBQUssRUFBRSxlQUFlO3lCQUN6Qjt3QkFDRDs0QkFDSSxTQUFTLEVBQUUsTUFBTTs0QkFDakIsUUFBUSxFQUFFLFFBQVE7NEJBQ2xCLEtBQUssRUFBRSxrQkFBa0I7NEJBQ3pCLEtBQUssRUFBRSxlQUFlO3lCQUN6Qjt3QkFDRDs0QkFDSSxTQUFTLEVBQUUsU0FBUzs0QkFDcEIsUUFBUSxFQUFFLE9BQU87NEJBQ2pCLEtBQUssRUFBRSxrQkFBa0I7NEJBQ3pCLEtBQUssRUFBRSxlQUFlO3lCQUN6QjtxQkFDSixDQUFDO29CQUNLLG9CQUFlLEdBQUcsRUFBRSxDQUFDO2dCQU1oQyxDQUFDO2dCQUpHLHVDQUFRLEdBQVIsVUFBUyxPQUFPO29CQUNaLElBQUksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDO29CQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUM3QixDQUFDO2dCQXhDTDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSx3QkFBd0I7d0JBQ2xDLFFBQVEsRUFBRSxtVEFTVDtxQkFDSixDQUFDOzt3Q0FBQTtnQkE2QkYsMkJBQUM7WUFBRCxDQTNCQSxBQTJCQyxJQUFBO1lBM0JELHVEQTJCQyxDQUFBIiwiZmlsZSI6InRlbXBsYXRlcy9jb250YWN0cy9jb250YWN0LWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ2FuZ3VsYXIyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NvbnRhY3QtbGlzdC1jb21wb25lbnQnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8aDM+Q290bmFjdCBsaXN0IGNvbXBvbmVudDwvaDM+XHJcbiAgICAgICAgPHVsICpuZ0Zvcj1cIiNjb250YWN0IG9mIGNvbnRhY3RzXCI+XHJcbiAgICAgICAgICAgIDxsaSAoY2xpY2spPVwib25TZWxlY3QoY29udGFjdClcIlxyXG4gICAgICAgICAgICAgICAgW2NsYXNzLmNsaWNrZWRdPVwic2hvd0RldGFpbCA9PSB0cnVlXCJcclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAge3tjb250YWN0LmZpcnN0TmFtZX19IHt7Y29udGFjdC5sYXN0TmFtZX19XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgPC91bD5cclxuICAgIGAsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ29udGFjdExpc3RDb21wb25lbnQge1xyXG4gICAgcHVibGljIGNvbnRhY3RzID0gW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmlyc3ROYW1lOiAnTWF4JyxcclxuICAgICAgICAgICAgbGFzdE5hbWU6ICdaaG92dHknLFxyXG4gICAgICAgICAgICBlbWFpbDogJ21heGltem92QGhvdG1haWwuY29tJyxcclxuICAgICAgICAgICAgcGhvbmU6ICcrNDUgNTAzMyA0NzgzJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBmaXJzdE5hbWU6ICdCaWxsJyxcclxuICAgICAgICAgICAgbGFzdE5hbWU6ICdJdmFub3YnLFxyXG4gICAgICAgICAgICBlbWFpbDogJ2JpbGxAaG90bWFpbC5jb20nLFxyXG4gICAgICAgICAgICBwaG9uZTogJys0NSAxMTIyIDExMjInXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpcnN0TmFtZTogJ0RtaXRyaXknLFxyXG4gICAgICAgICAgICBsYXN0TmFtZTogJ0JhbmFuJyxcclxuICAgICAgICAgICAgZW1haWw6ICdhbmR5QGhvdG1haWwuY29tJyxcclxuICAgICAgICAgICAgcGhvbmU6ICcrNDUgMzM0NCAzMzQ0J1xyXG4gICAgICAgIH1cclxuICAgIF07XHJcbiAgICBwdWJsaWMgc2VsZWN0ZWRDb250YWN0ID0ge307XHJcblxyXG4gICAgb25TZWxlY3QoY29udGFjdCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRDb250YWN0ID0gY29udGFjdDtcclxuICAgICAgICBjb25zb2xlLmxvZygnb24gc2VsZWN0Jyk7XHJcbiAgICB9XHJcbn0iXX0=
