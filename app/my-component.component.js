System.register(['angular2/core', "./contacts/contact-list.component", "angular2/router"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, contact_list_component_1, router_1;
    var MyComponentComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (contact_list_component_1_1) {
                contact_list_component_1 = contact_list_component_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            MyComponentComponent = (function () {
                function MyComponentComponent() {
                }
                MyComponentComponent.prototype.ngOnInit = function () {
                    this.name = 'Pussy';
                };
                MyComponentComponent = __decorate([
                    core_1.Component({
                        selector: 'my-component',
                        template: "\n        \n        <!--<div>Hi, I am <span [style.color]=\"'red'\">{{name}}</span></div>-->\n        <!--<br />-->\n        <!--<span [class.is-awesome]=\"inputElement.value === 'yes'\">Is it awesome?</span>-->\n        <!---->\n        <!--<input type=\"text\" #inputElement (keyup)=\"0\" />-->\n        <!--<br />-->\n        <!--<br />-->\n        <!--<button [disabled]=\"inputElement.value !== 'yes'\">Only if Yes</button>-->\n\n        <header>\n            <nav>\n                <a [routerLink]=\"['Contacts']\">Contacts</a>\n                <a [routerLink]=\"['NewContact']\">New contacts</a>\n            </nav>\n        </header>\n        \n        <main>\n            <router-outlet></router-outlet>\n        </main>\n    ",
                        styleUrls: [
                            'assets/scss/my-component.scss'
                        ],
                        directives: [contact_list_component_1.ContactListComponent, router_1.ROUTER_DIRECTIVES]
                    }),
                    router_1.RouteConfig([
                        { path: '/contacts', name: 'Contacts', component: contact_list_component_1.ContactListComponent },
                        { path: '/newcontact', name: 'NewContact', component: contact_list_component_1.ContactListComponent },
                    ]), 
                    __metadata('design:paramtypes', [])
                ], MyComponentComponent);
                return MyComponentComponent;
            }());
            exports_1("MyComponentComponent", MyComponentComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm15LWNvbXBvbmVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUF1Q0E7Z0JBQUE7Z0JBT0EsQ0FBQztnQkFIRyx1Q0FBUSxHQUFSO29CQUNJLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDO2dCQUN4QixDQUFDO2dCQXpDTDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxjQUFjO3dCQUN4QixRQUFRLEVBQUUsaXVCQXFCVDt3QkFDRCxTQUFTLEVBQUU7NEJBQ1AsK0JBQStCO3lCQUNsQzt3QkFDRCxVQUFVLEVBQUUsQ0FBQyw2Q0FBb0IsRUFBRSwwQkFBaUIsQ0FBQztxQkFDeEQsQ0FBQztvQkFFRCxvQkFBVyxDQUFDO3dCQUNULEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSw2Q0FBb0IsRUFBQzt3QkFDdkUsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLDZDQUFvQixFQUFDO3FCQUM5RSxDQUFDOzt3Q0FBQTtnQkFTRiwyQkFBQztZQUFELENBUEEsQUFPQyxJQUFBO1lBUEQsdURBT0MsQ0FBQSIsImZpbGUiOiJteS1jb21wb25lbnQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7Q29udGFjdExpc3RDb21wb25lbnR9IGZyb20gXCIuL2NvbnRhY3RzL2NvbnRhY3QtbGlzdC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtSb3V0ZUNvbmZpZywgUk9VVEVSX0RJUkVDVElWRVN9IGZyb20gXCJhbmd1bGFyMi9yb3V0ZXJcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdteS1jb21wb25lbnQnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICBcclxuICAgICAgICA8IS0tPGRpdj5IaSwgSSBhbSA8c3BhbiBbc3R5bGUuY29sb3JdPVwiJ3JlZCdcIj57e25hbWV9fTwvc3Bhbj48L2Rpdj4tLT5cclxuICAgICAgICA8IS0tPGJyIC8+LS0+XHJcbiAgICAgICAgPCEtLTxzcGFuIFtjbGFzcy5pcy1hd2Vzb21lXT1cImlucHV0RWxlbWVudC52YWx1ZSA9PT0gJ3llcydcIj5JcyBpdCBhd2Vzb21lPzwvc3Bhbj4tLT5cclxuICAgICAgICA8IS0tLS0+XHJcbiAgICAgICAgPCEtLTxpbnB1dCB0eXBlPVwidGV4dFwiICNpbnB1dEVsZW1lbnQgKGtleXVwKT1cIjBcIiAvPi0tPlxyXG4gICAgICAgIDwhLS08YnIgLz4tLT5cclxuICAgICAgICA8IS0tPGJyIC8+LS0+XHJcbiAgICAgICAgPCEtLTxidXR0b24gW2Rpc2FibGVkXT1cImlucHV0RWxlbWVudC52YWx1ZSAhPT0gJ3llcydcIj5Pbmx5IGlmIFllczwvYnV0dG9uPi0tPlxyXG5cclxuICAgICAgICA8aGVhZGVyPlxyXG4gICAgICAgICAgICA8bmF2PlxyXG4gICAgICAgICAgICAgICAgPGEgW3JvdXRlckxpbmtdPVwiWydDb250YWN0cyddXCI+Q29udGFjdHM8L2E+XHJcbiAgICAgICAgICAgICAgICA8YSBbcm91dGVyTGlua109XCJbJ05ld0NvbnRhY3QnXVwiPk5ldyBjb250YWN0czwvYT5cclxuICAgICAgICAgICAgPC9uYXY+XHJcbiAgICAgICAgPC9oZWFkZXI+XHJcbiAgICAgICAgXHJcbiAgICAgICAgPG1haW4+XHJcbiAgICAgICAgICAgIDxyb3V0ZXItb3V0bGV0Pjwvcm91dGVyLW91dGxldD5cclxuICAgICAgICA8L21haW4+XHJcbiAgICBgLFxyXG4gICAgc3R5bGVVcmxzOiBbXHJcbiAgICAgICAgJ2Fzc2V0cy9zY3NzL215LWNvbXBvbmVudC5zY3NzJ1xyXG4gICAgXSxcclxuICAgIGRpcmVjdGl2ZXM6IFtDb250YWN0TGlzdENvbXBvbmVudCwgUk9VVEVSX0RJUkVDVElWRVNdXHJcbn0pXHJcblxyXG5AUm91dGVDb25maWcoW1xyXG4gICAgeyBwYXRoOiAnL2NvbnRhY3RzJywgbmFtZTogJ0NvbnRhY3RzJywgY29tcG9uZW50OiBDb250YWN0TGlzdENvbXBvbmVudH0sXHJcbiAgICB7IHBhdGg6ICcvbmV3Y29udGFjdCcsIG5hbWU6ICdOZXdDb250YWN0JywgY29tcG9uZW50OiBDb250YWN0TGlzdENvbXBvbmVudH0sXHJcbl0pXHJcblxyXG5leHBvcnQgY2xhc3MgTXlDb21wb25lbnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXR7XHJcbiAgICBuYW1lOiBzdHJpbmc7XHJcblxyXG5cclxuICAgIG5nT25Jbml0KCk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5uYW1lID0gJ1B1c3N5JztcclxuICAgIH1cclxufSJdfQ==
