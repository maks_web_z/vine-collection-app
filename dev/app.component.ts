import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from "angular2/router";
import {VineListComponent} from "./vine-collection/vine-list.component";
import {VineDetailsComponent} from "./vine-collection/vine-details.component";
import {AddNewVineComponent} from "./vine-collection/add-new-vine.component";

@Component({
    selector: 'my-app',
    template: `
        <header>
            <div class="container">
                <div class="img-background"></div>
            </div>
        </header>        
        <main>
            <router-outlet></router-outlet>
        </main>
    `,
    directives: [VineListComponent, VineDetailsComponent, AddNewVineComponent, ROUTER_DIRECTIVES]
})

@RouteConfig([
    { path: '/vine-list', name: 'VineList', component: VineListComponent, useAsDefault: true},
    { path: '/vine-details/:vineId', name: 'VineDetails', component: VineDetailsComponent},
    { path: '/add-vine', name: 'AddNewVine', component: AddNewVineComponent},
])

export class AppComponent {
    name: string;

}