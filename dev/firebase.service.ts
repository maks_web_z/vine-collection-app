import {Injectable} from 'angular2/core'
import {Http} from 'angular2/http'
import 'rxjs/Rx'

@Injectable()
export class FirebaseService {

    constructor(private _http: Http) {}

    getVines(){
        return this._http.get('https://contact-app-a1b14.firebaseio.com/vine-collection.json')
            .map(response => response.json());
    }

    getSingleVine(key: string){
        return this._http.get('https://contact-app-a1b14.firebaseio.com/vine-collection/' + key +  '.json')
            .map(response => response.json());
    }

    addNewVine(vineName: string, category: string, description: string, price: string, imageUrl: string ) {
        const body = JSON.stringify({
            name: vineName,
            category: category,
            description: description,
            price: price,
            imageUrl: imageUrl
        });
        console.log(body);

        return this._http.post('https://contact-app-a1b14.firebaseio.com/vine-collection.json', body)
            .map(response => response.json());
    }

}