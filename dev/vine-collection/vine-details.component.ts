import {Component, OnInit} from 'angular2/core';
import {RouteParams} from "angular2/router";
import {FirebaseService} from "../firebase.service";

@Component({
    selector: 'vine-details',
    template: `
        <div class="vine-details-component" *ngIf="showElement == true">
            <div class="container">
                 <div class="row">
                     
                     <div class="col-md-4">
                         <div class="vine-img-wrapper">
                            <img class="vine-img img-responsive" src="{{ vineItem.imageUrl }}" />
                         </div>
                     </div>
                     
                     <div class="col-md-8">
                         
                         <div class="vine-info-wrapper">
                             <div class="vine-price">{{ vineItem.price }} kr.</div>
                             <div class="vine-description">{{ vineItem.description }}</div>
                             <div class="details">
                                 Categories: <span class="text-red">{{ vineItem.category }}</span>
                             </div>
                         </div>                         
                         
                     </div><!-- .col-md-8 -->
                     
                 </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .vine-details-component -->        
    `,
    providers: [FirebaseService]
})

export class VineDetailsComponent implements OnInit {

    constructor(private _routerParams: RouteParams, private _firebaseService: FirebaseService) {}

    public vineId: string;
    public vineItem: {};
    public showElement = false;

    ngOnInit(): any {
        this.vineId = this._routerParams.get('vineId');
        this.getSingleVine();
    }

    getSingleVine() {
        this._firebaseService.getSingleVine(this.vineId)
            .subscribe(
                response => {
                    this.vineItem = response;
                    this.showElement = true;
                },
                error => console.log(error)
            );
    }

}