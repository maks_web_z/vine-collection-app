import {Component } from 'angular2/core';
import {Router} from "angular2/router";

@Component({
    selector: 'single-vine',
    template: `
        <section class="vine-item">
            <img class="vine-img img-responsive" src="{{ vineItem.imageUrl }}" />
            <div class="vine-category">{{ vineItem.category }}</div>
            <div class="vine-name">{{ vineItem.name }}</div>
            <div class="price">{{ vineItem.price }} kr.</div>
            <button class="btn-details" (click)="showVineDetails()">Details</button>
        </section>
    `,
    inputs: ['vineItem']
})

export class SingleVineComponent {

    constructor(private _router: Router) {}

    public vineItem: {};

    showVineDetails(){
        this._router.navigate(['VineDetails', { vineId: this.vineItem.key }]);
    }
}