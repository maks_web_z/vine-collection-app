import {Component } from 'angular2/core';
import {Router} from "angular2/router";
import {ControlGroup} from 'angular2/common';
import {FirebaseService} from "../firebase.service";

@Component({
    selector: 'ann-new-vine',
    template: `
        <div class="add-new-vine-component">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-6">
                        <form (ngSubmit)="onSubmit(f)" #f="ngForm">

                            <div class="form-label">Name of the vine</div>
                            <input class="form-text-field" type="text" ngControl="vineName" /> <br />

                            <div class="form-label">Category</div>
                            <input class="form-text-field" type="text" ngControl="category" /> <br />

                            <div class="form-label">Description</div>
                            <textarea class="form-text-area description" rows="7" ngControl="description"></textarea>

                            <div class="form-label">Price</div>
                            <input class="form-text-field" type="text" ngControl="price" /> <br />

                            <div class="form-label">Image Url as ./src/vine-images/cold-wind.png</div>
                            <input class="form-text-field" type="text" ngControl="imageUrl" /> <br />

                            <button class="btn-submit" type="submit">Add new vine</button>
                        </form>
                    </div>                    
                </div>                
                
            </div><!-- .container -->
        </div><!-- .add-new-vine-component -->        
    `,
    providers: [FirebaseService]
})

export class AddNewVineComponent {

    public response: string;

    constructor(private _firebaseService: FirebaseService, private _router: Router) {}

    onSubmit(form: ControlGroup){
        this._firebaseService.addNewVine(form.value.vineName, form.value.category, form.value.description, form.value.price, form.value.imageUrl)
            .subscribe(
                response => {
                    this.response = JSON.stringify(response);
                    this._router.navigate(['VineList']);
                },
                error => console.log(error)
            );
    }

}