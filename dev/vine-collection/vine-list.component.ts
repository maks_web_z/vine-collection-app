import {Component, OnInit} from 'angular2/core';
import {SingleVineComponent} from "./single-vine.component";
import {FirebaseService} from "../firebase.service";

@Component({
    selector: 'contact-list-component',
    template: `        
        <div class="view-list-component">
            
            <div class="container">
                <div class="mini-heading">Enjoy your</div>
                <h1 class="heading">Vine collection</h1>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="list-description">
                            Choose your favorite from a great variety of wines for every price point and any occasion, from rich
                            shardonnay to elegant and creamy brut.
                        </div>
                    </div>
                </div><!-- .row -->
            </div><!-- .container -->

            <div class="container" *ngIf="showList == true">
                <div class="row">
                    
                        <div *ngFor="#vineItem of vineCollectionArray" class="col-md-3">
                            <single-vine [vineItem]="vineItem"></single-vine>
                        </div>

                </div><!-- .row -->
                
                <div class="add-new-vine-wrapper">
                    <a href="/add-vine" class="add-new-vine">Add new vine</a>
                </div>
                
            </div><!-- .container -->
            
        </div><!-- .view-list-component -->
    `,
    directives: [SingleVineComponent],
    providers: [FirebaseService]
})

export class VineListComponent implements OnInit {

    public vineCollectionObj: any;
    public vineCollectionArray = [];
    public showList = false;

    constructor(private _firebaseService: FirebaseService) {}

    ngOnInit(): any {
        this.getVineCollection();
    }

    getVineCollection() {
        this._firebaseService.getVines()
            .subscribe(
                response => {
                    this.vineCollectionObj = response;
                    this.transformToArray(this.vineCollectionObj);
                },
                error => console.log(error)
            );
    }

    transformToArray(vineCollection){

        for (var key in vineCollection) {
            this.vineCollectionArray.push({
                'key': key,
                'name': vineCollection[key].name,
                'category': vineCollection[key].category,
                'price': vineCollection[key].price,
                'imageUrl': vineCollection[key].imageUrl,
                'description': vineCollection[key].description
            });
        }

        this.showList = true;
    }

}